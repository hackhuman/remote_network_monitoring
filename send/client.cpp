#include "client.h"
#include "ui_client.h"
#include <QHostAddress>

client::client(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::client)
{
    ui->setupUi(this);

    tcpSocket = NULL;
    //fen pei kong jian zhi ding dui xiang
    tcpSocket = new QTcpSocket(this);

    connect (tcpSocket, &QTcpSocket::connected,
             [=]()
    {
        ui->textread->setText("connect succeed");
    }

             );

    connect(tcpSocket, &QTcpSocket::readyRead,
            [=]()
    {

            //get feil from server
            QByteArray array = tcpSocket->readAll();
            //writ in readtext
            ui->textread->append(array);



    }

            );
}

client::~client()
{
    delete ui;
}


void client::on_pushButton_clicked()
{
    //get server ip and port
    QString ip = ui->lineEditip->text();
    qint16 port = ui->lineEditport->text().toInt();
    //active connect to server
    tcpSocket->connectToHost(QHostAddress(ip),port);
}

void client::on_Buttonsend_clicked()
{   //get feil from sendtext
    QString str = ui->textsend->toPlainText();
    //send
    tcpSocket->write(str.toUtf8().data());

}

void client::on_Buttoncolse_clicked()
{
    //active disconnect
    tcpSocket->disconnectFromHost();
    tcpSocket->close();
}
