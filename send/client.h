#ifndef CLIENT_H
#define CLIENT_H

#include <QWidget>
#include <QTcpSocket>//tong xin socket
namespace Ui {
class client;
}

class client : public QWidget
{
    Q_OBJECT

public:
    explicit client(QWidget *parent = 0);
    ~client();

private slots:
    void on_pushButton_clicked();

    void on_Buttonsend_clicked();

    void on_Buttoncolse_clicked();

private:
    Ui::client *ui;
    QTcpSocket *tcpSocket;//tong xin socket

};

#endif // CLIENT_H
