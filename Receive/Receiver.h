#ifndef RECEIVER_H
#define RECEIVER_H

#include <QDialog>
#include <QUdpSocket>
#include <QTimer>
#include <QTime>

typedef struct _PackageHeader
{
    uint packet_header_size;
    uint packet_size;
    uint packet_num;
    uint package_size;
    uint package_Offset;
    uint count;
}PackageHeader;
typedef struct _UdpUnpackData
{
    char data[1000*1000];
    int length;
}UdpUnpackData;

namespace Ui {
class Receiver;
}

class Receiver : public QDialog
{
    Q_OBJECT

public:
    explicit Receiver(QWidget *parent = 0);
    ~Receiver();

private slots:
    void on_B_strat_clicked();
    void on_B_stop_clicked();
    void ReceiveImage();
    void resetsocket();

private:
    Ui::Receiver *ui;
    QUdpSocket * receiver;
    UdpUnpackData imageData;
    QHostAddress sender_ip;
    quint16 sender_port;
    QTimer *timer;
    bool state;

};


#endif // RECEIVER_H
