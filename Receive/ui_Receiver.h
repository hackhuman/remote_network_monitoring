/********************************************************************************
** Form generated from reading UI file 'Receiver.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RECEIVER_H
#define UI_RECEIVER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_Receiver
{
public:
    QLabel *label_image;
    QLabel *label_port;
    QLineEdit *lineEdit_port;
    QPushButton *B_strat;
    QPushButton *B_stop;

    void setupUi(QDialog *Receiver)
    {
        if (Receiver->objectName().isEmpty())
            Receiver->setObjectName(QStringLiteral("Receiver"));
        Receiver->resize(340, 301);
        label_image = new QLabel(Receiver);
        label_image->setObjectName(QStringLiteral("label_image"));
        label_image->setGeometry(QRect(0, 50, 341, 251));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_image->sizePolicy().hasHeightForWidth());
        label_image->setSizePolicy(sizePolicy);
        QPalette palette;
        QBrush brush(QColor(0, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        palette.setBrush(QPalette::Active, QPalette::Window, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush);
        label_image->setPalette(palette);
        label_image->setStyleSheet(QStringLiteral("background-color: rgb(0, 0, 0);"));
        label_port = new QLabel(Receiver);
        label_port->setObjectName(QStringLiteral("label_port"));
        label_port->setGeometry(QRect(10, 20, 41, 20));
        label_port->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        lineEdit_port = new QLineEdit(Receiver);
        lineEdit_port->setObjectName(QStringLiteral("lineEdit_port"));
        lineEdit_port->setGeometry(QRect(60, 10, 113, 27));
        B_strat = new QPushButton(Receiver);
        B_strat->setObjectName(QStringLiteral("B_strat"));
        B_strat->setGeometry(QRect(180, 10, 71, 27));
        B_stop = new QPushButton(Receiver);
        B_stop->setObjectName(QStringLiteral("B_stop"));
        B_stop->setGeometry(QRect(260, 10, 71, 27));

        retranslateUi(Receiver);

        QMetaObject::connectSlotsByName(Receiver);
    } // setupUi

    void retranslateUi(QDialog *Receiver)
    {
        Receiver->setWindowTitle(QApplication::translate("Receiver", "Receiver", 0));
        label_image->setText(QString());
        label_port->setText(QApplication::translate("Receiver", "Port:", 0));
        lineEdit_port->setText(QApplication::translate("Receiver", "5200", 0));
        B_strat->setText(QApplication::translate("Receiver", "Start", 0));
        B_stop->setText(QApplication::translate("Receiver", "Stop", 0));
    } // retranslateUi

};

namespace Ui {
    class Receiver: public Ui_Receiver {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RECEIVER_H
