#-------------------------------------------------
#
# Project created by QtCreator 2018-06-22T15:48:26
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Receiver
TEMPLATE = app


SOURCES += main.cpp\
        Receiver.cpp

HEADERS  += Receiver.h

FORMS    += Receiver.ui
